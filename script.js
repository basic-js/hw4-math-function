const getNumberA = () => {
    let a = prompt('Введіть число a');
    
    while ((a === '') || (a === null) || isNaN(Number(a))) {
        a = prompt('Це не число, введіть число a');
    }
    return Number(a);
}


const getNumberB = () => {
    let b = prompt('Введіть число b');
    
    while ((b === '') || (b === null) || isNaN(Number(b))) {
        b = prompt('Це не число, введіть число b');
    }
    return Number(b);
}


const mathFunction = (a, b) => {
    let result = '';
    let oper = prompt('Введіть операцію');

    switch (oper) {
        case '+':
            result = a + b;
            break;
        case '-':
            result = a - b;
            break;
        case '/':
            result = a / b;
            break;
        case '*':
            result = a * b;
            break;
        default:
            alert('Невідома операція');
    }
    return result;
}

console.log(mathFunction(getNumberA(), getNumberB()));